let burgerbtn = document.getElementById('burger-btn');
let burgerpopup = document.getElementById('popup-burger');
let toggle = true;

burgerbtn.addEventListener('click', () => {

    if(toggle) {
        burgerbtn.style.transform = "rotateY(180deg)";
        burgerpopup.style.display = "flex";
    }
    else {
        burgerbtn.style.transform = "rotateY(-180deg)";
        burgerpopup.style.display = "none";
    }
    toggle = !toggle;
})