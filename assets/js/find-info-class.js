let classe = localStorage.getItem('classe');
let fond = document.getElementById('fond');
let logoclass = document.getElementById('class-logo');
let classname = document.getElementById('class-name');
let imgspediv = document.getElementById('img-spes');
let imgspeelitediv = document.getElementById('img-spes-elite');
let loadingscreen = document.getElementById('loading-window');
let arbo = document.getElementById('class-name-arbo');

fetch(`https://api.guildwars2.com/v2/professions/${classe}?lang=fr`)
.then(function(res) {
    return res.json();
})
.then(function (transform) {
    console.log(transform);
    displayinfo(transform);
    fetchspe(transform);
});

const fetchspe = (json) => {
    for (let i = 0; i < json.specializations.length; i++) {
    fetch(`https://api.guildwars2.com/v2/specializations/${json.specializations[i]}?lang=fr`)
        .then(function(res) {
            return res.json();
        })
        .then(function (transform) {
            console.log(transform);
            displayspe(transform);
        });

    }
    setTimeout(function () {
        loadingscreen.style.display = 'none';
    }, 1500);
}

const displayspe = (json) => {
        let myimg = document.createElement("img");
        myimg.src = json.icon;
        myimg.title = 'spécialisation : ' + json.name;
        if(json.elite) {
            imgspeelitediv.append(myimg);
        } else {
            imgspediv.append(myimg);
        }
}

const displayinfo = (json) => {

    logoclass.src = json.icon_big;
    classname.innerText = json.name;
    arbo.innerText = json.name;

    switch (classe) {
        case 'Guardian':
            fond.style.backgroundImage =  "url('assets/images/classes/guardian.png')";
            break;
        case 'Warrior':
            fond.style.backgroundImage = "url('assets/images/classes/warrior.png')";
            break;
        case 'Engineer':
            fond.style.backgroundImage = "url('assets/images/classes/ingenieur.png')";
            break;
        case 'Elementalist':
            fond.style.backgroundImage = "url('assets/images/classes/elem.jpg')";
            break;
        case 'Mesmer':
            fond.style.backgroundImage = "url('assets/images/classes/mesmer.png')";
            break;
        case 'Ranger':
            fond.style.backgroundImage = "url('assets/images/classes/ranger.png')";
            break;
        case 'Thief':
            fond.style.backgroundImage = "url('assets/images/classes/voleur.jpg')";
            break;
        case 'Necromancer':
            fond.style.backgroundImage = "url('assets/images/classes/necromancer.png')";
            break;
    }
}
