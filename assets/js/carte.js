// Including the leaflet js library provides the variable L with various map tools, see "http://leafletjs.com/reference-1.0.0.html" for documentation

// Add leaflet css stylesheet (used for layer controls, marker styles and attribution)
$('<link>').appendTo('head').attr({type: 'text/css', rel: 'stylesheet', href: "https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.0-rc.1/leaflet.css" });

// This could be called anything, just bear in mind if you rename this then you need to rename anything with the prefix "map."
var map;

// Helper function to convert GW2 coordinates into Leaflet coordinates
//  GW2 coordinates: Northwest = [0,0], Southeast = [continent_xmax,continent_ymax];
//  Leaflet: Northwest = [0,0], Southeast = [-256, 256]
function unproject(coord) {
    return map.unproject(coord, map.getMaxZoom() );
}

// Helper function to print clicked coordinates to the log
function onMapClick(e) {
    console.log("You clicked the map at " + map.project(e.latlng,map.getMaxZoom()));
}

// Main map function
function createMap() {

    // Adds the leaflet map within the specified element, in this case a div with id="mapdiv"
    //  Additionally we set the zoom levels to match the tilelayers, and set the coordinate reference system (simple)
    //  In this case we're using the maximum zoom from Tyria as 7. It would be 6 for The Mists.
    map = L.map("mapdiv", {
        minZoom: 3,
        maxZoom: 7,
        crs: L.CRS.Simple
    });

    // Add map tiles using the [[API:Tile service]]
    L.tileLayer("https://tiles.guildwars2.com/1/1/{z}/{x}/{y}.jpg").addTo(map);

    // Restrict the area which can be panned to
    //  In this case we're using the coordinates for the continent of tyria from "https://api.guildwars2.com/v2/continents/1"
    let continent_dims = [49152,49152];
    let mapbounds = new L.LatLngBounds(unproject([0,0]), unproject(continent_dims)); // northwest, southeast
    map.setMaxBounds(mapbounds);

    // Set the default viewport position (in this case the midpoint) and zoom (in this case zoom level 1)
    map.setView(unproject([(continent_dims[0] / 3),(continent_dims[1] / 3)]), 3);

    // Add a function to return clicked coordinates to the javascript console
    map.on("click", onMapClick);

    // Add a tile layer
    map.addLayer( L.tileLayer("https://tiles.guildwars2.com/1/1/{z}/{x}/{y}.jpg", { minZoom: 0, maxZoom: 7, continuousWorld: true, subdomains: [1,2,3,4], bounds: mapbounds }) );

    //custom marker
    let gwicone = L.icon({
        iconUrl: 'assets/images/logo/marker.png',
        iconSize: [38, 38], // size of the icon
    });

    // Add some points to different marker groups
    let archedulion = L.marker(unproject([16448, 15168]), {icon: gwicone}, { title: "L'Arche du Lion" }).addTo(map);
    let promontoire = L.marker(unproject([11200, 10848]), {icon: gwicone}, { title: "Le Promontoire Divin" }).addTo(map);
    let ratasum = L.marker(unproject([5984, 20928]), {icon: gwicone}, { title: "Rata Sum" }).addTo(map);
    let bosquet = L.marker(unproject([10328, 21016]), {icon: gwicone}, { title: "Le Bosquet" }).addTo(map);
    let citadelle = L.marker(unproject([24440, 14320]), {icon: gwicone}, { title: "La Citadelle Noir" }).addTo(map);
    let hoelbrak = L.marker(unproject([21008, 14128]), {icon: gwicone}, { title: "Hoelbrak" }).addTo(map);

    archedulion.bindPopup("<b>L'Arche Du Lion</b><br><img src='assets/images/capitales/arche_du_lion.jpg' style='height: 150px'>", {
        maxWidth: "auto"
    });
    promontoire.bindPopup("<b>Le Promontoire Divin</b><br><img src='assets/images/capitales/promontoire_divin.jpg' style='height: 150px'>", {
        maxWidth: "auto"
    });
    ratasum.bindPopup("<b>Rata Sum</b><br><img src='assets/images/capitales/rata_sum.jpg' style='height: 150px'>", {
        maxWidth: "auto"
    });
    bosquet.bindPopup("<b>Le Bosquet</b><br><img src='assets/images/capitales/le_bosquet.jpg' style='height: 150px'>", {
        maxWidth: "auto"
    });
    citadelle.bindPopup("<b>La Citadelle Noire</b><br><img src='assets/images/capitales/citadelle_noir.jpg' style='height: 150px'>", {
        maxWidth: "auto"
    });
    hoelbrak.bindPopup("<b>Hoelbrak</b><br><img src='assets/images/capitales/hoelbrak.jpg' style='height: 150px'>", {
        maxWidth: "auto"
    });

}
createMap();

